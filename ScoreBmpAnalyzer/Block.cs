﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBmpAnalyzer
{

    class Block
    {
        public int Top { get; private set; }
        public int Left { get; private set; }
        public int Bottom { get; private set; }
        public int Right { get; private set; }

        public int Width { get { return Right - Left + 1; } }
        public int Height { get { return Bottom - Top + 1; } }

        public int NoteColumnMax { get { return Notes.Count > 0 ? Notes.Max(n => n.Column + 1) : 0; } }

        public static int MaxMeasureLength { get; set; }
        public static int MaxMargin { get; set; }

        private PixelColor[] ColorData;
        private List<Note> Notes;

        private int NumOfColumns;
        public int NumOfBar { get; private set; }
        public int MeasureOffset { get; set; }


        public Block(PixelArray img, int x, int y, List<PixelColor> additionalColor)
        {
            Fit(img, x, y, additionalColor);
            ColorData = new PixelColor[Width * Height];
            CopyImg(img);
            Notes = new List<Note>();
        }

        #region Methods of Block Class

        public void Fit(PixelArray img, int xo, int yo, List<PixelColor> additionalColor)
        {
            Edge top = new Edge(yo - 1, -1, false),
                bottom = new Edge(yo + 1, +1, false),
                left = new Edge(xo - 1, -1, true),
                right = new Edge(xo + 1, +1, true);

            top.LinkEdges(left, right);
            bottom.LinkEdges(left, right);
            left.LinkEdges(top, bottom);
            right.LinkEdges(top, bottom);

            Edge[] edges = { bottom, left, right };
            // don't have to judge the top edge

            bool continueFlag = true;

            while (continueFlag)
            {
                continueFlag = false;

                if (left.Position < 0) left.Position = 0;
                if (right.Position >= img.Width) right.Position = img.Width - 1;
                if (top.Position < 0) top.Position = 0;
                if (bottom.Position >= img.Height) bottom.Position = img.Height - 1;

                // Inclement the edge if it has a non-black pixel
                foreach (var eg in edges)
                {
                    if (eg.Expand(img, additionalColor))
                    {
                        continueFlag = true;
                    }
                }
                // End the loop if all of pixels at edge are white
            }
            Top = top.Position + 1;
            Bottom = bottom.Position - 1;
            Left = left.Position + 1;
            Right = right.Position - 1;
        }

        /// <summary>
        /// Copy the color data from the loaded BMP to the field
        /// </summary>
        /// <param name="img">BMP source</param>
        private void CopyImg(PixelArray img)
        {
            for (int y = Top; y <= Bottom; y++)
            {
                for (int x = Left; x <= Right; x++)
                {
                    ColorData[Access2D(x - Left, y - Top)]
                        = new PixelColor(img.GetColor(x, y));
                }
            }
        }

        private int Access2D(int x, int y)
        {
            return y * Width + x;
        }

        public void MarkPosition(int[] table, int imgWidth)
        {
            for (int y = Top; y <= Bottom; y++)
            {
                table[y * imgWidth + Left] = Width;
            }
        }

        public void ReadNotes()
        {
            var columns = new int[Width];

            var ruler = new int[Height];

            var excludedColors = new List<PixelColor>();

            excludedColors.Add(new PixelColor(255, 0, 0, 255));
            excludedColors.Add(new PixelColor(0, 255, 0, 255));

            NumOfColumns = 0;

            int blackStart = -1;
            bool blackDetected = false;
            for (int x = 0; x < Width; x++)
            {
                if (ColorData[Access2D(x, 0)].IsBlack(excludedColors))
                {
                    if (!blackDetected)
                    {
                        blackDetected = true;
                        blackStart = x;
                    }
                    //columns[NumOfColumns++] = x;
                }
                else
                {
                    if (blackDetected)
                    {
                        columns[NumOfColumns++] = (blackStart + x-1) / 2;
                        blackDetected = false;
                    }
                }
            }

            NumOfBar = 0;
            /*
            {
                int x = 0;
                while (NumOfBar == 0)
                {
                    NumOfBar = 0;

                    for (int y = Height - 1; y >= 0; y--)
                    {
                        if (ColorData[Access2D(x, y)].IsBlack(excludedColors))
                        {
                            NumOfBar++;
                        }
                        ruler[y] = NumOfBar;
                    }
                    if (ruler.GroupBy(value => value)
                        .Max(eachGroup => eachGroup.Count())
                        > MaxMeasureLength)
                    {
                        NumOfBar = 0;
                        x++;
                        if (x > MaxMargin)
                        {
                            throw new Exception("Bitmap cannot be analyzed");
                            //TODO
                        }
                    }
                }
            }*/
            for (int x = 0; x < MaxMargin; x++)
            {
                NumOfBar = 0;

                for (int y = Height - 1; y >= 0; y--)
                {
                    //gridDetector
                    if (ColorData[Access2D(x, y)].IsBlack(excludedColors))
                    {
                        NumOfBar++;
                    }
                    //gridDetector

                    ruler[y] = NumOfBar;
                }
                if (NumOfBar <= 0
                    || ruler.GroupBy(value => value)
                    .Max(eachGroup => eachGroup.Count())
                    > MaxMeasureLength)
                {
                    System.Diagnostics.Debug.WriteLine(x.ToString());
                    if (x >= MaxMargin - 1)
                    {
                        throw new Exception("Bitmap cannot be analyzed");
                        //TODO
                    }
                    continue;
                }
                break;
            }

            Notes.Clear();

            int offset;

            var state = new Status();

            for (int c = 0; c < NumOfColumns; c++)
            {
                offset = Height - 1;

                state = Status.Idle;

                for (int y = Height - 1; y >= 0; y--)
                {
                    if (y > 0 && ruler[y - 1] != ruler[y])
                    {
                        offset = y - 1;
                    }
                    if (y == Height - 1
                        && !ColorData[Access2D(columns[c], y)]
                                .IsBlack(excludedColors))
                    {
                        state = Status.Detected;
                    }

                    switch (state)
                    {
                        case Status.Idle:
                            if (!ColorData[Access2D(columns[c], y)]
                                .IsBlack(excludedColors))
                            {
                                state = Status.Start;
                            }
                            break;
                        case Status.Start:
                            if (!ColorData[Access2D(columns[c], y)]
                                .IsNear(ColorData[Access2D(columns[c], y + 1)]))
                            {
                                Notes.Add(new Note(ruler[y], -y + offset, c));
                                state = Status.Detected;
                            }
                            break;
                        case Status.Detected:
                            if (ColorData[Access2D(columns[c], y)]
                                .IsBlack(excludedColors))
                            {
                                state = Status.Idle;
                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// The state machine for the notes detector
        /// </summary>
        private enum Status
        {
            Idle, Start, Detected
        }

        public void WriteAscii(System.IO.StreamWriter sr)
        {
            Notes = Notes
                .OrderBy(note => note.Measure)
                .ThenBy(note => note.Position)
                .ToList();

            foreach (var note in Notes)
            {
                sr.Write((note.Measure + this.MeasureOffset).ToString());
                sr.Write(",");

                sr.Write(note.Position.ToString());
                sr.Write(",");

                sr.Write(note.Column.ToString());
                sr.Write(",");

                sr.Write("\r\n");
            }
        }

        public void PutNotes(int[] table, int width, int maxClap, int clapSize)
        {
            int height = table.Length / width;
            foreach (var n in Notes)
            {
                //TODO
                int idx = (height
                    - ((n.Measure + MeasureOffset - 1) * maxClap
                    + n.Position / clapSize) - 1)
                    * width + n.Column;
                table[idx] = 1;
            }
        }

        /// <summary>
        /// Debug code
        /// </summary>
        /// <param name="img"></param>
        /// <param name="color"></param>
        /// <param name="id"></param>
        public void DrawId(PixelArray img, PixelColor color, int id)
        {
            for (int yb = Top; yb <= Bottom; yb++)
            {
                for (int xb = Left; xb <= Right; xb++)
                {
                    if (xb == Left || xb == Right
                        || yb == Top || yb == Bottom
                        || ((Bottom - yb) % 2 == 0 && (Bottom - yb) <= id * 2)
                        )
                    {
                        img.SetColor(xb, yb, color);
                    }
                }
            }
        }

        #endregion

        #region Inner Class for Block

        /// <summary>
        /// Information of the note
        /// </summary>
        private class Note
        {
            public int Measure { get; private set; }
            public int Position { get; private set; }
            public int Column { get; private set; }

            public Note(int measure, int position, int column)
            {
                Measure = measure;
                Position = position;
                Column = column;
            }
        }

        /// <summary>
        /// The class to detect the four sides of the block
        /// </summary>
        private class Edge
        {
            // using struct is not effective for performance
            public int Position { get; set; }
            public int ExpandDirection { get; private set; }
            private Edge StartEdge, EndEdge;
            private bool Vertical;

            public Edge(int pos, int expandDirection, bool vertical)
            {
                Position = pos;
                ExpandDirection = expandDirection;
                Vertical = vertical;
            }
            public bool Expand(PixelArray img, List<PixelColor> additionalColor)
            {
                bool expandFlag = false;

                for (int p = StartEdge.Position; p < EndEdge.Position; p++)
                {
                    while (!img.GetColor(
                        Vertical ? Position : p,
                        Vertical ? p : Position).IsWhite(additionalColor))
                    {
                        Position += ExpandDirection;
                        if (!img.Contains(Vertical ? Position : p,
                            Vertical ? p : Position))
                        {
                            Position -= ExpandDirection;
                            break;
                        }
                        if (!expandFlag)
                        {
                            expandFlag = true;
                        }
                    }
                }
                return expandFlag;
            }

            public void LinkEdges(Edge start, Edge end)
            {
                StartEdge = start;
                EndEdge = end;
            }
        }

        #endregion
    }
}
