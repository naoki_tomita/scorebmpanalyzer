﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ScoreBmpAnalyzer
{
    class Controller
    {
        private IView OverView;


        public Controller(IView view)
        {
            OverView = view;
        }

        public bool Analyze(string[] path)
        {
            if (path.Length == 1)
            {
                var worker = new Worker();
                if (!worker.ProcessAll(path[0], OverView))
                {
                    return false;
                }
                return worker.TryImgSave();
            }

            int failureCount = 0;
            var workers = new List<Worker>();

            /*
            foreach (var p in path)
            {
                var worker = new Worker();
                if (worker.ProcessAll(p, OverView))
                {
                    workers.Add(worker);
                }
                else
                {
                    failureCount++;
                }
            }*/

            
            Task.WaitAll(path.Select(p => Task.Run(() =>
            {
                var worker = new Worker();
                if (worker.ProcessAll(p, OverView))
                {
                    workers.Add(worker);
                }
                else
                {
                    failureCount++;
                }
            })).ToArray());
            

            return (workers.Count(w => !(w.TryImgSave())) == 0 && failureCount == 0);
        }
    }
    class Worker
    {
        MainAnalyzer Analyzer;
        string Directory;
        string Filename;
        private IView OverView;

        public bool ProcessAll(string path, IView view)
        {
            OverView = view;

            Directory = System.IO.Path.GetDirectoryName(path) + @"\save\";
            Filename = System.IO.Path.GetFileNameWithoutExtension(path);

            try
            {
                Analyzer = new MainAnalyzer(path);
            }
            catch (Exception excep)
            {
                OverView.IndicateText("File cannot open:\n" + excep.ToString());
                return false;
            }

            Analyzer.DetectBlocks();

            Analyzer.ReadNotes();
            Analyzer.DrawBlockId(2);


            if (!System.IO.Directory.Exists(Directory))
            {
                System.IO.Directory.CreateDirectory(Directory);
            }
            /*
            try
            {*/
            //Analyzer.Save(Directory + Filename + "_block.png");

            Analyzer.WriteCsv(Directory + Filename + "_notes.csv");
            Analyzer.DecodeNotes(Directory + Filename + "_result.csv");
            /*
            }
            catch (Exception excep)
            {
                OverView.IndicateText("Save failure:\n" + excep.ToString());
                return false;
            }*/
            return true;
        }
        public bool TryImgSave()
        {
            try
            {
                Analyzer.Save(Directory + Filename + "_block.png");
            }
            catch (Exception excep)
            {
                OverView.IndicateText("Save failure:\n" + excep.ToString());
                return false;
            }
            return true;
        }
    }

    public interface IView
    {
        void IndicateText(string text);
    }
}
