﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ScoreBmpAnalyzer
{
    public partial class Form1 : Form, IView 
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();

            dialog.Filter =
                "Image Files (*.bmp, *.jpg, *.png)|*.bmp;*.jpg;*.png|All Files (*.*)|*.*";
            dialog.Multiselect = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var sw = new Stopwatch();
                sw.Start();

                if (new Controller(this).Analyze(dialog.FileNames))
                {
                    sw.Stop();
                    MessageBox.Show(sw.ElapsedMilliseconds.ToString());
                    this.Close();
                }
                sw.Stop();
            }
        }

        public void IndicateText(string text){
            MessageBox.Show(text);
        }
    }
}
