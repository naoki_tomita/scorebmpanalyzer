﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBmpAnalyzer
{
    class MainAnalyzer
    {
        private PixelArray Img;
        private List<Block> Blocks;
        private static int MinimumBlockWidth = 96;
        private static int MinimumBlockHeight = 32;
        private static int MaxBlockMargin = 32;
        private static int MeasureLengthLimit = 192;
        private static int BlockOverlapThreshold = 192;
        private static int ClapsInMeasure = 32;
        private int maxMeasure;

        public MainAnalyzer(string path)
        {
            Block.MaxMeasureLength = MeasureLengthLimit;
            Block.MaxMargin = MaxBlockMargin;
            Img = new PixelArray(path);
            Blocks = new List<Block>();
        }
        public void Save(string path)
        {
            Img.SaveBmp(path);
        }

        public void WriteCsv(string path)
        {
            using (var sr =
                new System.IO.StreamWriter(path, false,
                    new System.Text.UTF8Encoding(false)))
            //.Encoding.GetEncoding("Shift_JIS")))
            {
                Blocks.ForEach(blk => blk.WriteAscii(sr));
            }
        }

        /// <summary>
        /// Debug code
        /// </summary>
        /// <param name="path"></param>
        public void DecodeNotes(string path)
        {
            //TODO
            int width = Blocks.Max(blk => blk.NoteColumnMax);
            int height = maxMeasure * ClapsInMeasure;
            var score = new int[width * height];

            foreach (var blk in Blocks)
            {
                blk.PutNotes(score, width, ClapsInMeasure, 4);//TODO
            }
            using (var sr =
                new System.IO.StreamWriter(path, false,
                    new System.Text.UTF8Encoding(false)))
            {
                for (int i = 0; i < score.Length; i++)
                {
                    sr.Write(score[i].ToString());
                    sr.Write(",");

                    if (i % width == width - 1)
                    {
                        sr.Write("\r\n");
                    }

                }
            }
        }

        /// <summary>
        /// Detect a Block frame as white rectangle
        /// </summary>
        public void DetectBlocks()
        {
            var excludedColors = new List<PixelColor>();

            excludedColors.Add(new PixelColor(255, 0, 0, 255));

            // Get the color of measure numbers
            new Action(() =>
            {
                for (int x = 0; x < Img.Width / 4; x++)
                {
                    for (int y = Img.Height - 1; y > Img.Height / 4; y--)
                    {
                        if (!Img.GetColor(x, y).IsNear(255, 255, 255, 255)
                            && !Img.GetColor(x, y).IsNear(excludedColors[0])
                            && !Img.GetColor(x, y).IsNear(0, 0, 0, 255))
                        {
                            excludedColors.Add(new PixelColor(Img.GetColor(x, y)));
                            return; // break multiloop
                        }
                    }
                }
            })();
            //http://perutago.seesaa.net/article/272310449.html

            var checkTable = new int[Img.Height * Img.Width];

            //http://feather.cocolog-nifty.com/weblog/2010/05/post-a00a.html

            for (int y = 0; y < Img.Height; y++)
            {
                for (int x = 0; x < Img.Width; x++)
                {
                    // x may be rewritten in the loop
                    if (checkTable[y * Img.Width + x] > 0)
                    {
                        x += checkTable[y * Img.Width + x];
                        if (x >= Img.Width)
                        {
                            break;
                        }
                    }
                    if (!Img.GetColor(x, y).IsWhite(excludedColors))
                    {
                        // Detect Block and add to the list
                        // Copy color data to the Block
                        var blk = new Block(Img, x, y, excludedColors);

                        // the detected area is no more checked
                        blk.MarkPosition(checkTable, Img.Width);

                        // a small block is not added
                        if (blk.Height > MinimumBlockHeight
                            && blk.Width > MinimumBlockWidth)
                        {
                            Blocks.Add(blk);
                        }

                        x = blk.Right + 1;
                    }
                }
            }
        }

        /// <summary>
        /// Debug code
        /// </summary>
        /// <param name="mode"></param>
        public void DrawBlockId(int mode)
        {
            //test
            int id = 0;
            var color = new PixelColor(255, 0, 0, 255);

            switch (mode)
            {
                case 1:
                    Task.WaitAll(Blocks.Select(blk => Task.Run(() =>
                               {
                                   blk.DrawId(Img, color, id);
                                   id++;
                               })).ToArray());
                    break;

                case 2:
                    foreach (var blk in Blocks)
                    {
                        blk.DrawId(Img, color, id);
                        id++;
                    }
                    break;
                case 3:
                    foreach (var blk in Blocks.AsParallel().ToArray())
                    {
                        blk.DrawId(Img, color, id);
                        id++;
                    }
                    break;
            }
        }

        public void ReadNotes()
        {
            Task.WaitAll(Blocks
                .Select(blk => Task.Run(() => blk.ReadNotes()))
                .ToArray());
            SortBlocks();
        }

        /// <summary>
        /// Sort blocks and set measure order
        /// </summary>
        public void SortBlocks()
        {
            var blockGroups = new List<VerticalPosition>();
            Blocks = Blocks.OrderBy(blk =>
            {
                int index = 0;
                foreach (var pos in blockGroups)
                {
                    if (pos.OverlapWith(blk.Top, blk.Bottom,
                        (blk.Height > BlockOverlapThreshold) ?
                        BlockOverlapThreshold : blk.Height - 4))
                    {
                        if (blk.Top < pos.Top)
                        {
                            pos.Top = blk.Top;
                        }
                        if (blk.Bottom < pos.Bottom)
                        {
                            pos.Bottom = blk.Bottom;
                        }
                        return index;
                    }
                    index++;
                }
                blockGroups.Add(new VerticalPosition(blk.Top, blk.Bottom));
                return index;
            }).ThenBy(b => b.Right).ToList();

            //Blocks = Blocks.OrderBy(b => b.Bottom).ThenBy(b=>-b.Right).ToList();

            maxMeasure = 0;
            foreach (var blk in Blocks)
            {
                blk.MeasureOffset = maxMeasure;
                maxMeasure += blk.NumOfBar;
            }
        }
        private class VerticalPosition
        {
            public int Top { get; set; }
            public int Bottom { get; set; }
            public VerticalPosition(int top, int bottom)
            {
                Top = top;
                Bottom = bottom;
            }
            public bool OverlapWith(int top, int bottom, int threshold)
            {
                return (bottom >= this.Top + threshold
                    && top <= this.Bottom - threshold);
            }
        }
    }
}
