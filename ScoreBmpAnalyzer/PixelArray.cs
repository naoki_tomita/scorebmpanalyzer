﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;

namespace ScoreBmpAnalyzer
{
    public class PixelArray
    {
        //http://xptn.dtiblog.com/blog-entry-99.html

        private byte[] PixelData;
        private PixelColor[] ColorData;
        public int Width { get; private set; }
        public int Height { get; private set; }

        /// <exception cref="System.IO.FileNotFoundException">
        /// Thrown when an attempt to access a file that does not exist on disk fails.
        /// </exception>
        public PixelArray(string path)
        {
            var img = new Bitmap(path);

            Width = img.Width;
            Height = img.Height;

            var bmpdat = img.LockBits(new Rectangle(0, 0, Width, Height),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

            PixelData = new byte[Width * Height * 4];

            Marshal.Copy(bmpdat.Scan0, PixelData, 0, PixelData.Length);

            img.UnlockBits(bmpdat);

            ColorData = new PixelColor[Width * Height];

            for (int i = 0; i < Width * Height; i++)
            {
                ColorData[i] = new PixelColor(
                    PixelData[i * 4 + 2],
                    PixelData[i * 4 + 1],
                    PixelData[i * 4 + 0],
                    PixelData[i * 4 + 3]);
            }
        }

        public void SaveBmp(string path)
        {
            var result = new Bitmap(Width, Height);

            var bmpdat = result.LockBits(new Rectangle(Point.Empty, new Size(Width, Height)),
                ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            Marshal.Copy(PixelData, 0, bmpdat.Scan0, PixelData.Length);

            result.UnlockBits(bmpdat);

            result.Save(path);
        }
        public PixelColor GetColor(int x, int y)
        {
            if (!Contains(x, y))
            {
                return new PixelColor(0, 0, 0, 0);
            }
            return ColorData[(y * Width + x)];
        }

        public void SetColor(int x, int y, byte r, byte g, byte b, byte a)
        {
            if (!Contains(x, y))
            {
                return;
            }
            ColorData[(y * Width + x)].SetColor(r, g, b, a);

            PixelData[(y * Width + x) * 4 + 2] = r;
            PixelData[(y * Width + x) * 4 + 1] = g;
            PixelData[(y * Width + x) * 4 + 0] = b;
            PixelData[(y * Width + x) * 4 + 3] = a;
        }

        public void SetColor(int x, int y, PixelColor pc)
        {
            SetColor(x, y, pc.R, pc.G, pc.B, pc.A);
        }

        public bool Contains(int x, int y)
        {
            return (x >= 0 && x < Width && y >= 0 && y < Height);
        }
    }

    public struct PixelColor
    {
        //http://msdn.microsoft.com/ja-jp/library/ms229031%28v=vs.100%29.aspx

        public byte R { get; private set; }
        public byte G { get; private set; }
        public byte B { get; private set; }
        public byte A { get; private set; }

        public static int ColorCompareThreshold = 4;
        public static byte BlackThreshold = 10;
        public static byte WhiteThreshold = 240;

        public PixelColor(byte r, byte g, byte b, byte a)
            : this()
        {
            SetColor(r, g, b, a);
        }
        public PixelColor(PixelColor org)
            : this()
        {
            SetColor(org.R, org.G, org.B, org.A);
        }
        public void SetColor(byte r, byte g, byte b, byte a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public bool IsNear(byte r, byte g, byte b, byte a)
        {
            return (MathUtil.IsNear((int)R, (int)r, ColorCompareThreshold)
                && MathUtil.IsNear((int)G, (int)g, ColorCompareThreshold)
                && MathUtil.IsNear((int)B, (int)b, ColorCompareThreshold)
                && MathUtil.IsNear((int)A, (int)a, ColorCompareThreshold));
        }
        public bool IsNear(PixelColor c)
        {
            return IsNear(c.R, c.G, c.B, c.A);
        }

        public bool IsDarker(byte r, byte g, byte b, byte a)
        {
            return (R <= r && G <= g && B <= b && A <= a);
        }
        public bool IsDarker(byte r, byte g, byte b)
        {
            return IsDarker(r, g, b, 0xFF);
        }
        public bool IsLighter(byte r, byte g, byte b, byte a)
        {
            return (R >= r && G >= g && B >= b && A >= a);
        }
        public bool IsLighter(byte r, byte g, byte b)
        {
            return IsLighter(r, g, b, 0x00);
        }

        public bool IsBlack(List<PixelColor> exclusion)
        {
            if (R == 255 && G == 255 && B == 255)
            {
                return false;
            }
            if (R == 0 && G == 0 && B == 0)
            {
                return true;
            }
            if (IsDarker(BlackThreshold, BlackThreshold, BlackThreshold))
            {
                return true;
            }
            foreach (var c in exclusion)
            {
                if (IsNear(c))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsWhite(List<PixelColor> additional)
        {
            if (R == 255 && G == 255 && B == 255)
            {
                return true;
            }
            if (R == 0 && G == 0 && B == 0)
            {
                return false;
            }
            if (IsLighter(WhiteThreshold, WhiteThreshold, WhiteThreshold, 0))
            {
                return true;
            }
            foreach (var c in additional)
            {
                if (IsNear(c))
                {
                    return true;
                }
            }
            return false;
        }

        public byte[] GetRGBA()
        {
            return new byte[] { R, G, B, A };
        }
    }
}
